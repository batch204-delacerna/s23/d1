//console.log("Hello, B204!");

/*
	Objects
		-An object is a data type that is used to represent a real world object
		-It is a collection of related data and/or functionalities
		-Information is stored in object represented in "key: value" pair
			key -> property of the object
			value -> actual data to be stored
		- Different data types may be stored in an object's property creating complex data structures

		Two ways of creating object in javascript
			1. Object Literal Notation 
				let/const objectName = {}

			2. Object Constructor Notation
				Object Instantiation ( let object = new Object() )
	
	Object Literal Notation
		- This creates/declares an object and also initializes/assigns it's properties upon creation
		- A cellphone is an example of a real world object
		- It has it's own properties such as name, color, weight, unit model and a lot of other things

		Syntax:
			let/const = {
				keyA: value,
				keyB: valueB
			};
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects using literal notation");
console.log(cellphone);

console.log(typeof cellphone);


let cellphone2 = {
	name: "Motorola",
	manufactureDate: 2000
};

console.log(cellphone2);


//Creating objects using constructor function
/*
	- Creates a reusable function to create several objects that have the same data structure
	- This is useful for creating multiple instances/copies of an object
	- An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
	 

	
	Syntax:
		function ObjectName(keyA, keyB) {
			this.keyA = keyA;
			this.keyB = keyB;
		}

*/

// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters
// The reason we USE capital case or pascal case here is to 
// DIFFERENTIATE it from a regular FX.


//THIS WILL BE THE TEMPLATE//
//THAT WE CAN USE ON AND ON AND ON//
function Laptop(name, manufactureDate) {
	//this.propertyName(KEY)     =     //value
	this.nameProperty = name;
	this.manufactureDateProperty = manufactureDate;
}

// This is a unique instance of the Laptop object
/*
    - The "new" operator creates an instance of an object
    - Objects and instances are often interchanged because object literals (let object = {}) and instances (let object = new object) are distinct/unique objects
*/


//REFER TO THE "TEMPLATE" ABOVE//
	//with NEW keyword
	let laptop = new Laptop('Lenovo', 2008);
	console.log(laptop);

	//without NEW keyword
	let oldLaptop = Laptop("IBM", 1980);
	console.log(oldLaptop); //undefined

	let myLaptop = new Laptop('MacBook', 2020);
	console.log(myLaptop);

	//Creating Empty Objects
	let computer = {};
	console.log(computer);

	//Creating Empty Objects other way
	let myComputer = new Object();
	console.log(myComputer);
//==========================================================
//JUST TO COMPARE IT WITH A REGULAR FUNCTION WITH RETURN
function student(name) {
	return name
}	
student("Mark");                   //This will not RETURN anything unless you store it is a variable.
let studentName = student("Mark"); //This will work because we stored it in a variable.
//==========================================================


//==========================================================
//Accessing Object Properties 2 ways
	//1 using dot notation
	//2 using square bracket notation


	//1 dot notation 
	console.log(myLaptop.nameProperty); 			 //Macbook
	console.log(myLaptop.manufactureDateProperty);   //2020

	//2 square bracket notation
	console.log(myLaptop["nameProperty"]);

//Accessing Array Objects
let array = [laptop, myLaptop]; 					//MacBook
console.log(array);

//Accessing Array inside an Array
console.log(array[1].nameProperty);					//MacBook//dot notation
console.log(array[1]['nameProperty']);				//MacBook//square bracker notation

//Initializing/Adding/Deleting/Reassigning Object Properties
let car = {};
console.log(car);  									//empty object
	
//ADDING	
//adding using dot notation
car.name = "Sarao";
console.log('Result of adding a property to an empty object');
console.log(car);

//adding using square bracket notation
//advisable for 2+ words property 
//but usually camelCase is the recommended
car['manufacture date'] = 2020;
console.log(car);

// Reassigning object properties
car.name = "Mustang";
console.log(car);

// Object Methods
/*
	-A method is a function which is a property of an object
	-They are also functions and one of the key differences they have is that methods are functions related to a specific object
	-Methods are useful for creating object specific function which are used to perform tasks on them.
*/

let person = {
	name: 'Jack',
	talk: function(){
		console.log('Hello! My name is ' + this.name);
	}
}
console.log(person);
person.talk();

person.walk = function() {
	console.log(this.name + "walked 25 steps forward.");
}

person.walk();

// Methods are useful for creating reusable functions that performs tasks related to object

let friend = {
	firstName: 'Rafael',
	lastName: 'Santillan',
	isMarried: true,
	address: {
		city: 'Quezon City',
		country: 'Philippines'
	},
	email: ['raf@mail.com', 'raf123@yahoo.com'],
	introduce: function(){
		console.log('Hello! My name is ' + this.firstName +' ' + this.lastName);
	}
}

friend.introduce();

//Real World Application of Objects
/*
	Scenario
		1. We would like to create a game that would have several pokemon interact with each other
		2. Every pokemon would have the same set of stats, properties, and functions.
*/

let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,

	tackle: function(){
		console.log("This Pokemon tackle target pokemon");
		console.log("targetPokemon's health is now reduced to targetPokemonHealth");
	},

	faint: function() {
		console.log("Pokemon fainted.");
	}
}

console.log(myPokemon);


// Creating an object constructor instead will help with this process
function Pokemon(name, level, health) {
	//properties
	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = level;

	/*
	snorlax.tackle({name: 'Squirtle', level: 99, health: 200, attack: 99});

	*/
	//methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));

		target.health -= this.attack 
		//target.health = target.health - this.attack
		//current health of target


		//if health is less than or equal to 5, invoke faint function
		if(target.health <= 5) {
			target.faint()
		}

	},

	this.faint = function() {
		console.log(this.name + " fainted");
	}
}

/*
let snorlax = {
	name: 'Snorlax', 
	level: 75, 
	health: 500, 
	attack: 75, 
};

let squirtle = {
	name: 'Squirtle', 
	level: 99, 
	health: 200, 
	attack: 99
}
*/

// Creates new instances of the "Pokemon" object each with their unique properties
let squirtle = new Pokemon("Squirtle", 99, 200);
let snorlax = new Pokemon("Snorlax", 75, 500);

console.log(squirtle);
console.log(snorlax);

//object.method(target)
snorlax.tackle(squirtle);
snorlax.tackle(squirtle);
snorlax.tackle(squirtle);
/*

Mini-Activity: 20 mins. 8: 50PM
1. Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function

*/
